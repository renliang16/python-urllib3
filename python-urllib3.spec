%global srcname urllib3
%bcond_without tests

Name:           python-%{srcname}
Version:        1.26.7
Release:        1
Summary:        Sanity-friendly HTTP client for Python
License:        MIT
URL:            https://urllib3.readthedocs.io
Source0:        https://github.com/urllib3/urllib3/archive/%{version}/%{version}.tar.gz
Source1:        ssl_match_hostname_py3.py

Patch0001:      remove_mock.patch
Patch6000:      backport-CVE-2021-28363.patch

BuildArch:      noarch

%description 
HTTP library with thread-safe connection pooling, file post support, sanity friendly, and more. 

%package -n     python3-%{srcname}
Summary:        Python3 HTTP library with thread-safe connection pooling and file post

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%if %{with tests}
BuildRequires:  python3-cryptography python3-mock python3-six python-idna python-dateutil
BuildRequires:  python3-pysocks python3-pytest python3-tornado python-trustme
%endif

Requires:       ca-certificates python3-idna python3-six python3-pysocks

%description -n python3-urllib3
Python3 HTTP module with connection pooling and file POST abilities.

%prep
%autosetup -n %{srcname}-%{version} -p1
#Failure use case circumvention
rm -rf test/with_dummyserver/
rm -rf test/appengine/
rm -rf test/contrib/
rm -rf test/test_no_ssl.py

%build
%py3_build

%install
%py3_install

rm -rf %{buildroot}%{python3_sitelib}/urllib3/packages/six.py
rm -rf %{buildroot}%{python3_sitelib}/urllib3/packages/__pycache__/six.*
rm -rf %{buildroot}%{python3_sitelib}/urllib3/packages/ssl_match_hostname/

mkdir -p %{buildroot}/%{python3_sitelib}/urllib3/packages/
cp -a %{SOURCE1} %{buildroot}/%{python3_sitelib}/urllib3/packages/ssl_match_hostname.py
ln -s %{python3_sitelib}/six.py %{buildroot}/%{python3_sitelib}/urllib3/packages/six.py
ln -s %{python3_sitelib}/__pycache__/six.cpython-%{python3_version_nodots}.opt-1.pyc %{buildroot}/%{python3_sitelib}/urllib3/packages/__pycache__/
ln -s %{python3_sitelib}/__pycache__/six.cpython-%{python3_version_nodots}.pyc %{buildroot}/%{python3_sitelib}/urllib3/packages/__pycache__/

%if %{with tests}
%check
# skip some failing tests
skiplist+="test_retry_deprecated or test_retry"
PYTHONPATH=%{buildroot}%{python3_sitelib}:%{python3_sitelib} %{__python3} -m pytest -v -k "not (${skiplist})"
%endif

%files -n python3-urllib3
%defattr(-,root,root)
%license LICENSE.txt
%doc CHANGES.rst README.rst
%{python3_sitelib}/urllib3/
%{python3_sitelib}/urllib3-*.egg-info

%changelog
* Wed Jan 19 2022 wangkerong <wangkerong@huawei.com> - 1.26.7-1
- update to 1.26.7 and remove python-nose dependency

* Mon Jul 5 2021 zhanzhimin <zhanzhimin@huawei.com> - 1.26.3-3
- fix CVE-2021-33503

* Tue Jun 1 2021 hanhui <hanhui15@huawei.com> - 1.26.3-2
- fix CVE-2021-28363

* Wed Feb 3 2021 chengguipeng <chengguipeng@huawei.com> - 1.26.3-1
- upgrade to 1.26.3

* Tue Jul 28 2020 chengguipeng<chengguipeng@huawei.com> - 1.25.9-1
- upgrade to 1.25.9

* Sat Oct 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.24.3-2
- optimize spec file.

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.24.3-1
- Package init
